#!/usr/bin/env python3
import time
import json
import requests
import yaml
import os

# Configuration
nomad_api_endpoint = "http://localhost:4646"
consul_api_endpoint = "http://localhost:8500"
cloudflared_config_path = "/etc/cloudflared/config.yml"
nomad_broadcast_ip = "192.168.86.37"
nomad_node_name = "mordor"
watch_interval = 30  # seconds

def get_nomad_allocations():
    """Get the list of allocations from the Nomad API."""
    response = requests.get(f"{nomad_api_endpoint}/v1/allocations")
    return response.json()

def get_consul_services():
    """Get the list of services running via the Consul API"""
    response = requests.get(f"{consul_api_endpoint}/v1/catalog/node/{nomad_node_name}")
    return response.json()

def get_dynamic_port(job_name, services):
    """Extract dynamic port from a Nomad allocation for the given job."""
    for service in services.items():
        if service[1]['Service'] == job_name:
            return service[1]['Port']

    return 0

def update_cloudflared_config(job_name, dynamic_port):
    """Update the cloudflared config file with the new dynamic port."""
    # Load the existing config
    with open(cloudflared_config_path, 'r') as file:
        config = yaml.safe_load(file)

    # Find the relevant ingress rule and update its port
    for ingress in config['ingress']:
        if 'hostname' in ingress and job_name in ingress['hostname']:
            ingress['service'] = f"http://{ nomad_broadcast_ip }:{ dynamic_port }"

    # Write the updated config back to file
    with open(cloudflared_config_path, 'w') as file:
        yaml.dump(config, file)

def reload_cloudflared_service():
    """Reload the cloudflared service to pick up the new configuration."""
    os.system('systemctl restart cloudflared')

# Main watcher loop
while True:
    # Get the list of allocations
    allocations = get_nomad_allocations()
    dynamic_services = get_consul_services()

    with open(cloudflared_config_path, 'r') as file:
        before = yaml.safe_load(file)

    # Iterate through the allocations
    for alloc in allocations:
        # If the app is not running, don't change anything
        if alloc['ClientStatus'] != 'running':
            continue

        # Get the dynamic port for the allocation
        job_name = alloc['JobID']
        dynamic_port = get_dynamic_port(job_name, dynamic_services['Services'])

        # Update the cloudflared config with the new port
        update_cloudflared_config(job_name, dynamic_port)

    with open(cloudflared_config_path, 'r') as file:
        after = yaml.safe_load(file)

    # Reload the cloudflared service if the config changed
    if before != after:
        reload_cloudflared_service()

    # Wait before checking again
    time.sleep(watch_interval)

