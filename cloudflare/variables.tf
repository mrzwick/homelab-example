variable "apps" {
  type = list(object({
    name = string
    destination = string
    port = number
  }))
  default = []
}

variable "internal_apps" {
  type    = list(string)
  default = []
}

variable "public_authenticated_apps" {
  type    = list(string)
  default = []
}

variable "tunnel_name" {
  type = string
}

variable "sso_login_url" {
  type = string
}

variable "cloudflare_account_id" {
  type = string
}

variable "cloudflare_api_token" {
  type = string
}

variable "cloudflare_auth_method" {
  type        = string
  description = "Name of an auth IdP that has already been setup in Cloudflare"
}

variable "cloudflare_zone" {
  type = string
}

variable "cloudflare_zone_id" {
  type    = string
}
