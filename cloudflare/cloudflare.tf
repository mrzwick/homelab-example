# Generates a 35-character secret for the tunnel.
resource "random_id" "tunnel_secret" {
  byte_length = 35
}

# Creates a new locally-managed tunnel for the GCP VM.
resource "cloudflare_tunnel" "auto_tunnel" {
  account_id = var.cloudflare_account_id
  name       = var.tunnel_name
  secret     = random_id.tunnel_secret.b64_std
}

resource "cloudflare_access_application" "app" {
  for_each   = {
    for index, app in var.apps:
    app.name => app
  }
  name                       = each.value.name
  custom_deny_url            = var.sso_login_url
  account_id                 = var.cloudflare_account_id
  domain                     = "${each.key}.${var.cloudflare_zone}"
  auto_redirect_to_identity  = true
  http_only_cookie_attribute = true
  allowed_idps               = [data.cloudflare_access_identity_provider.auto0.id]
}

resource "cloudflare_access_group" "admin_group" {
  name    = "${title(var.tunnel_name)} Admin"
  account_id = var.cloudflare_account_id
  include {
    login_method = [data.cloudflare_access_identity_provider.auto0.id]
  }

  require {
    # There should be an OIDC claim here
  }
}

resource "cloudflare_access_group" "internal_group" {
  name       = "${title(var.tunnel_name)} User"
  account_id = var.cloudflare_account_id
  include {
    login_method = [data.cloudflare_access_identity_provider.auto0.id]
  }

  require {
    # There should be an OIDC claim here
  }
}

resource "cloudflare_access_group" "public_authenticated_group" {
  name       = "${title(var.tunnel_name)} Guest"
  account_id = var.cloudflare_account_id
  include {
    login_method = [data.cloudflare_access_identity_provider.auto0.id]
  }
}

resource "cloudflare_access_policy" "app_policy" {
  for_each       = cloudflare_access_application.app
  application_id = each.value.id
  decision       = "allow"
  name           = "Allow Admins"
  precedence     = 1
  account_id     = var.cloudflare_account_id
  include {
    group = [cloudflare_access_group.admin_group.id]
  }
}

resource "cloudflare_access_policy" "internal_app_policy" {
  for_each       = {
    for k, v in cloudflare_access_application.app : k => v
    if contains(var.internal_apps, v.name)
  }

  application_id = each.value.id
  decision       = "allow"
  name           = "Allow Internal Authenticated"
  precedence     = 10
  account_id     = var.cloudflare_account_id
  include {
    group = [cloudflare_access_group.internal_group.id]
  }
}

resource "cloudflare_access_policy" "public_authenticated_app_policy" {
  for_each       = {
    for k, v in cloudflare_access_application.app : k => v
    if contains(var.public_authenticated_apps, v.name)
  }

  application_id = each.value.id
  decision       = "allow"
  name           = "Allow Public Authenticated"
  precedence     = 20
  account_id     = var.cloudflare_account_id
  include {
    group = [cloudflare_access_group.public_authenticated_group.id]
  }
}

# Creates the CNAME record that routes ssh_app.${var.cloudflare_zone} to the tunnel.
resource "cloudflare_record" "app" {
  for_each   = {
    for index, app in var.apps:
    app.name => app
  }
  zone_id  = var.cloudflare_zone_id
  name     = "${each.value.name}.${var.cloudflare_zone}"
  value    = "${cloudflare_tunnel.auto_tunnel.id}.cfargotunnel.com"
  type     = "CNAME"
  proxied  = true
}

data "cloudflare_access_identity_provider" "auto0" {
  name       = var.cloudflare_auth_method
  account_id = var.cloudflare_account_id
}

