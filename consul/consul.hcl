datacenter       = "mordor"
data_dir         = "/var/data/consul"
log_level        = "info"
encrypt          = "YOUR_KEY_HERE"
server           = true
ui               = true
bootstrap_expect = 1
bind_addr        = "127.0.0.1"
client_addr      = "0.0.0.0"

addresses = {
  https = "127.0.0.1"
  grpc  = "127.0.0.1"
}

connect {
  enabled = true
}

ports {
  grpc = 8502
}