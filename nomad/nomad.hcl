datacenter = "mordor"
data_dir   = "/var/data/nomad"

bind_addr = "0.0.0.0"

server {
  enabled          = true
  bootstrap_expect = 1
}

plugin "docker" {
  config {
#    extra_labels = ["job_name", "task_group_name", "task_name", "namespace", "node_name"]
    volumes {
      enabled = true
    }
  }
}

client {
  enabled = true
}

telemetry {
  collection_interval        = "1s"
  disable_hostname           = true
  prometheus_metrics         = true
  publish_allocation_metrics = true
  publish_node_metrics       = true
}


