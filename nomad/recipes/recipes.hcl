job "recipes" {
  datacenters = ["mordor"]

  type = "service"

  group "application" {
    count = 1

    service {
      name = "recipes"
      port = "http"
    }

    network {
      port "http" {
        to = 80
      }
    }

    task "server" {
      driver = "docker"

      resources {
        memory = 1024
      }

      env {
        TZ   = "America/New_York"
        PGID = "1000"
        PUID = "1000"
      }

      config {
        image = "hkotel/mealie:latest"
        ports = ["http"]

        volumes = [
          "/var/data/nomad/mealie:/app/data",
        ]

        force_pull = true
      }
    }
  }
}
